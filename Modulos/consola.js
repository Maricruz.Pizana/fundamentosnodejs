var table = [
    {
        a: 1,
        b: 'Z'
    },
    {
        a: 2,
        b: 'Otro valor'
    }
]

//CONSOLE LOG
console.log('Este es un console.log');

//CONSOLE INFO
console.info('Este es un console.info');

//CONSOLE ERROR
console.error('Este es un console.error');

//CONSOLE WARN
console.warn('Este es un console.warn');

//CONSOLE TABLE
console.table(table);

//CONSOLE GROUP
console.group('Conversación: ');
console.log('Hello');
console.log('How r u?');
console.log('Well');
console.log('Byeeee');
console.groupEnd('Conversación: ')

//CONSOLE COUNT
console.count('Numero');
console.count('Numero');
console.countReset('Numero');
console.count('Numero');
console.count('Numero');