async function hola(nombre) {
    return new Promise(function (resolve,reject){
        setTimeout(function () {
            console.log('Hello '+ nombre);
            resolve(nombre);
        }, 1500);
    });
}

function hablar (callbackHablar) {
    return new Promise (function (resolve, reject){
        setTimeout(function () {
            console.log('Bla bla bla bla...');
                resolve('Hay un errorsss');
        }, 1000);
    });   
} 


function adios(nombre) {
    return new Promise ((resolve, reject) => {
        setTimeout(function () {
            console.log('Bye '+ nombre);
            resolve(nombre);
        }, 1000);
    });  
}

async function main (){
   await hola('Maree'); 
}

main();